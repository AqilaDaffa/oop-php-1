<?php
    trait Fight{
        public $attackPower;
        public $defencePower;

        public function atraksi(){
            echo "{$this->nama} Sedang {$this->skill}";
        }
    }

    abstract class Hewan{
        use Fight;
        public $darah = 50;
        public $jumlahKaki;
        public $skill;

        public function serang($hewan){
            echo "{$this->nama} sedang menyerang {$hewan->nama}";
        }

        public function diserang($hewan){
            echo "{$this->nama} diserang {$hewan->nama}";

            $this->darah -= $hewan->attackPower / $this->deffencePower;
        }

        protected function getInfo(){
            echo "<br>";
            echo "Nama: {$this->nama}";
            echo "<br>";
            echo "Jumlah Kaki: {$this->jumlahKaki}";
            echo "<br>";
            echo "Keahlian: {$this->skill}";
            echo "<br>";
            echo "Darah: {$this->darah}";
            echo "<br>";
            echo "Attack Power: {$this->attackPower}";
            echo "<br>";
            echo "Deffence Power: {$this->deffencePower}";
            echo "<br>";
            $this->atraksi();
        }
    }

    class Harimau extends Hewan{
        public function __construct($nama){
            $this->nama = $nama;
            $this->jumlahKaki = 4;
            $this->skill = "lari cepat";
            $this->attackPower = 7;
            $this->deffencePower = 8;
        }

        public function getInfoHewan(){
            echo "Jenis Hewan: Terbang";
            $this->getInfo();
        }
    }

    class Elang extends Hewan{
        public function __construct($nama){
            $this->nama = $nama;
            $this->jumlahKaki = 2;
            $this->skill = "terbang tinggi";
            $this->attackPower = 10;
            $this->deffencePower = 5;
        }

        public function getInfoHewan(){
            echo "Jenis Hewan: Darat";
            $this->getInfo();
        }
    }

    class baris{
        public static function buatbaris(){
            echo "<br>";
            echo "================================================================";
            echo "<br>";
        }
    }

    $elang = new Elang('Elang');
    $elang->getInfoHewan();

    baris::buatbaris();

    $harimau = new Harimau('Harimau');
    $harimau->getInfoHewan();

    baris::buatbaris();

    $elang->serang($harimau);

    baris::buatbaris();

    $harimau->diserang($elang);

    baris::buatbaris();

    $harimau->getInfoHewan();

    baris::buatbaris();

    $elang->getInfoHewan();



?>
